Julia Version 1.1.1
Commit 55e36cc308 (2019-05-16 04:10 UTC)
Platform Info:
  OS: Linux (x86_64-pc-linux-gnu)
  CPU: Intel(R) Core(TM) i7-4600U CPU @ 2.10GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-6.0.1 (ORCJIT, haswell)
Date and time: 2019-8-29, 14:33:56
Ntrain = 721
Ntest = 309
Nruns = 10
Nguesses = 3
k = 5
random_seed = 0
rng_lam = [
  1.000000e-18
  1.000000e-17
  1.000000e-16
  1.000000e-15
  1.000000e-14
  1.000000e-13
  1.000000e-12
  1.000000e-11
  1.000000e-10
  1.000000e-09
  1.000000e-08
  1.000000e-07
  1.000000e-06
  1.000000e-05
  1.000000e-04
]
rng_sig = [
  3.906250e-03
  7.812500e-03
  1.562500e-02
  3.125000e-02
  6.250000e-02
  1.250000e-01
  2.500000e-01
  5.000000e-01
  1.000000e+00
  2.000000e+00
  4.000000e+00
  8.000000e+00
  1.600000e+01
  3.200000e+01
  6.400000e+01
  1.280000e+02
  2.560000e+02
  5.120000e+02
  1.024000e+03
  2.048000e+03
  4.096000e+03
  8.192000e+03
  1.638400e+04
  3.276800e+04
  6.553600e+04
  1.310720e+05
  2.621440e+05
  5.242880e+05
  1.048576e+06
  2.097152e+06
  4.194304e+06
  8.388608e+06
  1.677722e+07
  3.355443e+07
  6.710886e+07
  1.342177e+08
  2.684355e+08
]
rng_p = [
  1.062500e+00
  1.125000e+00
  1.250000e+00
  1.500000e+00
  2.000000e+00
  3.000000e+00
  5.000000e+00
  9.000000e+00
  1.700000e+01
]
m = 8
r = 3

KERNEL RIDGE REGRESSION

 RUN 1 OF 10
  testing parameters ... 307.797992 secs
  sigma: 6.400000e+01
  lambda: 1.000000e-07
  cv error: 0.152897
  test error: 0.148477
 RUN 2 OF 10
  testing parameters ... 334.415370 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.148086
  test error: 0.157266
 RUN 3 OF 10
  testing parameters ... 331.729266 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.143662
  test error: 0.161236
 RUN 4 OF 10
  testing parameters ... 326.468596 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.153474
  test error: 0.155917
 RUN 5 OF 10
  testing parameters ... 325.786246 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.146945
  test error: 0.152870
 RUN 6 OF 10
  testing parameters ... 322.146583 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.153604
  test error: 0.147862
 RUN 7 OF 10
  testing parameters ... 315.785479 secs
  sigma: 1.280000e+02
  lambda: 1.000000e-05
  cv error: 0.156805
  test error: 0.143550
 RUN 8 OF 10
  testing parameters ... 320.551754 secs
  sigma: 1.280000e+02
  lambda: 1.000000e-06
  cv error: 0.148225
  test error: 0.149750
 RUN 9 OF 10
  testing parameters ... 315.906356 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.155175
  test error: 0.145662
 RUN 10 OF 10
  testing parameters ... 324.472174 secs
  sigma: 1.280000e+02
  lambda: 1.000000e-05
  cv error: 0.158373
  test error: 0.148119
 MEAN VALUES
  cv error: 0.151725
  test error: 0.151071

MULTIDIMENSIONAL FOURIER REGRESSION

 RUN 1 OF 10:
  testing parameters ... 1232.089884 secs
  m: 8
  p: 1.500000
  r: 3
  lambda: 1.000000e-09
  cv error: 0.140526
  test error: 0.137629
 RUN 2 OF 10:
  testing parameters ... 1192.283316 secs
  m: 8
  p: 1.125000
  r: 3
  lambda: 1.000000e-09
  cv error: 0.141054
  test error: 0.161023
 RUN 3 OF 10:
  testing parameters ... 1179.672859 secs
  m: 8
  p: 1.125000
  r: 3
  lambda: 1.000000e-05
  cv error: 0.136911
  test error: 0.158814
 RUN 4 OF 10:
  testing parameters ... 1169.463762 secs
  m: 8
  p: 1.500000
  r: 3
  lambda: 1.000000e-15
  cv error: 0.142828
  test error: 0.151195
 RUN 5 OF 10:
  testing parameters ... 1166.262445 secs
  m: 8
  p: 1.500000
  r: 3
  lambda: 1.000000e-10
  cv error: 0.136138
  test error: 0.159788
 RUN 6 OF 10:
  testing parameters ... 1188.148530 secs
  m: 8
  p: 1.250000
  r: 3
  lambda: 1.000000e-05
  cv error: 0.141659
  test error: 0.152039
 RUN 7 OF 10:
  testing parameters ... 1191.356693 secs
  m: 8
  p: 1.250000
  r: 3
  lambda: 1.000000e-11
  cv error: 0.144364
  test error: 0.136197
 RUN 8 OF 10:
  testing parameters ... 1149.808956 secs
  m: 8
  p: 1.062500
  r: 3
  lambda: 1.000000e-04
  cv error: 0.144080
  test error: 0.142863
 RUN 9 OF 10:
  testing parameters ... 1061.134576 secs
  m: 8
  p: 1.250000
  r: 3
  lambda: 1.000000e-11
  cv error: 0.142772
  test error: 0.150716
 RUN 10 OF 10:
  testing parameters ... 1056.431899 secs
  m: 8
  p: 1.125000
  r: 3
  lambda: 1.000000e-08
  cv error: 0.144005
  test error: 0.138282
 MEAN VALUES
  cv error: 0.141434
  test error: 0.148855

RANDOM FOURIER REGRESSION

 RUN 1 OF 10
  testing parameters ... 165.792746 secs
  sigma: 3.200000e+01
  lambda: 1.000000e-06
  D: 480
  cv error: 0.155113
  test error: 0.153076
 RUN 2 OF 10
  testing parameters ... 168.929221 secs
  sigma: 6.400000e+01
  lambda: 1.000000e-05
  D: 480
  cv error: 0.150921
  test error: 0.156772
 RUN 3 OF 10
  testing parameters ... 169.283136 secs
  sigma: 3.200000e+01
  lambda: 1.000000e-06
  D: 480
  cv error: 0.151619
  test error: 0.171683
 RUN 4 OF 10
  testing parameters ... 169.179067 secs
  sigma: 6.400000e+01
  lambda: 1.000000e-05
  D: 480
  cv error: 0.156500
  test error: 0.161233
 RUN 5 OF 10
  testing parameters ... 168.270747 secs
  sigma: 1.600000e+01
  lambda: 1.000000e-07
  D: 480
  cv error: 0.151917
  test error: 0.159836
 RUN 6 OF 10
  testing parameters ... 169.104383 secs
  sigma: 6.400000e+01
  lambda: 1.000000e-05
  D: 480
  cv error: 0.157621
  test error: 0.163142
 RUN 7 OF 10
  testing parameters ... 167.974609 secs
  sigma: 3.200000e+01
  lambda: 1.000000e-06
  D: 480
  cv error: 0.161329
  test error: 0.145135
 RUN 8 OF 10
  testing parameters ... 166.952547 secs
  sigma: 6.400000e+01
  lambda: 1.000000e-06
  D: 480
  cv error: 0.152420
  test error: 0.157485
 RUN 9 OF 10
  testing parameters ... 166.804105 secs
  sigma: 6.400000e+01
  lambda: 1.000000e-05
  D: 480
  cv error: 0.161679
  test error: 0.157976
 RUN 10 OF 10
  testing parameters ... 166.540926 secs
  sigma: 3.200000e+01
  lambda: 1.000000e-06
  D: 480
  cv error: 0.160101
  test error: 0.156543
 MEAN VALUES
  cv error: 0.155922
  test error: 0.158288
