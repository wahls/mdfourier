Julia Version 0.2.1
Commit e44b593* (2014-02-11 06:30 UTC)
Platform Info:
  System: Linux (x86_64-linux-gnu)
  WORD_SIZE: 64
  BLAS: libblas.so.3
  LAPACK: liblapack.so.3
  LIBM: libopenlibm
Date and time: 2014-10-9, 20:9:58
Ntrain = 721
Ntest = 309
Nruns = 10
Nguesses = 3
k = 5
random_seed = 0
rng_lam = [
  1.000000e-18
  1.000000e-17
  1.000000e-16
  1.000000e-15
  1.000000e-14
  1.000000e-13
  1.000000e-12
  1.000000e-11
  1.000000e-10
  1.000000e-09
  1.000000e-08
  1.000000e-07
  1.000000e-06
  1.000000e-05
  1.000000e-04
]
rng_sig = [
  3.906250e-03
  7.812500e-03
  1.562500e-02
  3.125000e-02
  6.250000e-02
  1.250000e-01
  2.500000e-01
  5.000000e-01
  1.000000e+00
  2.000000e+00
  4.000000e+00
  8.000000e+00
  1.600000e+01
  3.200000e+01
  6.400000e+01
  1.280000e+02
  2.560000e+02
  5.120000e+02
  1.024000e+03
  2.048000e+03
  4.096000e+03
  8.192000e+03
  1.638400e+04
  3.276800e+04
  6.553600e+04
  1.310720e+05
  2.621440e+05
  5.242880e+05
  1.048576e+06
  2.097152e+06
  4.194304e+06
  8.388608e+06
  1.677722e+07
  3.355443e+07
  6.710886e+07
  1.342177e+08
  2.684355e+08
]
rng_p = [
  1.062500e+00
  1.125000e+00
  1.250000e+00
  1.500000e+00
  2.000000e+00
  3.000000e+00
  5.000000e+00
  9.000000e+00
  1.700000e+01
]
m = 8
r = 3

KERNEL RIDGE REGRESSION

 RUN 1 OF 10
  testing parameters ... 701.320872 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.152227
  test error: 0.145866
 RUN 2 OF 10
  testing parameters ... 683.987034 secs
  sigma: 1.280000e+02
  lambda: 1.000000e-06
  cv error: 0.146067
  test error: 0.153244
 RUN 3 OF 10
  testing parameters ... 738.237335 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.151402
  test error: 0.143001
 RUN 4 OF 10
  testing parameters ... 667.297729 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.143706
  test error: 0.158005
 RUN 5 OF 10
  testing parameters ... 706.582426 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.152429
  test error: 0.148267
 RUN 6 OF 10
  testing parameters ... 702.485301 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.155147
  test error: 0.136795
 RUN 7 OF 10
  testing parameters ... 670.855875 secs
  sigma: 1.280000e+02
  lambda: 1.000000e-05
  cv error: 0.151040
  test error: 0.163668
 RUN 8 OF 10
  testing parameters ... 666.693598 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.153415
  test error: 0.142074
 RUN 9 OF 10
  testing parameters ... 721.393559 secs
  sigma: 2.560000e+02
  lambda: 1.000000e-05
  cv error: 0.149022
  test error: 0.140911
 RUN 10 OF 10
  testing parameters ... 672.174273 secs
  sigma: 1.280000e+02
  lambda: 1.000000e-05
  cv error: 0.155184
  test error: 0.148810
 MEAN VALUES
  cv error: 0.150964
  test error: 0.148064

MULTIDIMENSIONAL FOURIER REGRESSION

 RUN 1 OF 10:
  testing parameters ... 2946.896225 secs
  m: 8
  p: 1.062500
  r: 3
  lambda: 1.000000e-04
  cv error: 0.139707
  test error: 0.136956
 RUN 2 OF 10:
  testing parameters ... 3091.901240 secs
  m: 8
  p: 1.250000
  r: 3
  lambda: 1.000000e-11
  cv error: 0.139011
  test error: 0.154264
 RUN 3 OF 10:
  testing parameters ... 3120.207029 secs
  m: 8
  p: 1.125000
  r: 3
  lambda: 1.000000e-10
  cv error: 0.140639
  test error: 0.155584
 RUN 4 OF 10:
  testing parameters ... 3107.889647 secs
  m: 8
  p: 1.062500
  r: 3
  lambda: 1.000000e-13
  cv error: 0.134205
  test error: 0.154846
 RUN 5 OF 10:
  testing parameters ... 3155.929061 secs
  m: 8
  p: 1.250000
  r: 3
  lambda: 1.000000e-05
  cv error: 0.144626
  test error: 0.139195
 RUN 6 OF 10:
  testing parameters ... 3070.407327 secs
  m: 8
  p: 1.250000
  r: 3
  lambda: 1.000000e-05
  cv error: 0.147858
  test error: 0.132262
 RUN 7 OF 10:
  testing parameters ... 3019.145456 secs
  m: 8
  p: 1.250000
  r: 3
  lambda: 1.000000e-05
  cv error: 0.139126
  test error: 0.158124
 RUN 8 OF 10:
  testing parameters ... 3078.249449 secs
  m: 8
  p: 1.125000
  r: 3
  lambda: 1.000000e-06
  cv error: 0.145461
  test error: 0.148325
 RUN 9 OF 10:
  testing parameters ... 3185.908397 secs
  m: 8
  p: 1.250000
  r: 3
  lambda: 1.000000e-09
  cv error: 0.146459
  test error: 0.139081
 RUN 10 OF 10:
  testing parameters ... 3038.829425 secs
  m: 8
  p: 1.125000
  r: 3
  lambda: 1.000000e-07
  cv error: 0.145859
  test error: 0.144766
 MEAN VALUES
  cv error: 0.142295
  test error: 0.146340

RANDOM FOURIER REGRESSION

 RUN 1 OF 10
  testing parameters ... 365.659560 secs
  sigma: 3.200000e+01
  lambda: 1.000000e-06
  D: 480
  cv error: 0.155937
  test error: 0.160806
 RUN 2 OF 10
  testing parameters ... 407.351407 secs
  sigma: 3.200000e+01
  lambda: 1.000000e-07
  D: 480
  cv error: 0.150759
  test error: 0.183086
 RUN 3 OF 10
  testing parameters ... 328.924470 secs
  sigma: 6.400000e+01
  lambda: 1.000000e-05
  D: 480
  cv error: 0.155349
  test error: 0.151694
 RUN 4 OF 10
  testing parameters ... 331.040058 secs
  sigma: 3.200000e+01
  lambda: 1.000000e-06
  D: 480
  cv error: 0.152652
  test error: 0.158480
 RUN 5 OF 10
  testing parameters ... 327.462578 secs
  sigma: 6.400000e+01
  lambda: 1.000000e-05
  D: 480
  cv error: 0.153100
  test error: 0.156179
 RUN 6 OF 10
  testing parameters ... 331.044230 secs
  sigma: 3.200000e+01
  lambda: 1.000000e-06
  D: 480
  cv error: 0.161865
  test error: 0.144196
 RUN 7 OF 10
  testing parameters ... 328.178964 secs
  sigma: 3.200000e+01
  lambda: 1.000000e-06
  D: 480
  cv error: 0.152037
  test error: 0.164643
 RUN 8 OF 10
  testing parameters ... 327.672477 secs
  sigma: 1.600000e+01
  lambda: 1.000000e-07
  D: 480
  cv error: 0.160364
  test error: 0.158861
 RUN 9 OF 10
  testing parameters ... 330.158249 secs
  sigma: 6.400000e+01
  lambda: 1.000000e-06
  D: 480
  cv error: 0.154438
  test error: 0.148252
 RUN 10 OF 10
  testing parameters ... 326.535426 secs
  sigma: 3.200000e+01
  lambda: 1.000000e-06
  D: 480
  cv error: 0.157403
  test error: 0.161580
 MEAN VALUES
  cv error: 0.155390
  test error: 0.158778
