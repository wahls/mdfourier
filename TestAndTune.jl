# Copyright 2014, 2019 Sander Wahls
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module TestAndTune

export test, grid_search2

using Printf

function test(evaluate::Function, dataset::Matrix{Float64})
# computes the error that evaluate achieves on dataset
    N,q = size(dataset)
    d = q - 1
    err = 0.0::Float64
    sqnrm = 0.0::Float64
    for i = 1:N
        yhat = evaluate(vec(dataset[i,1:d]))
        err += abs2((dataset[i,end] - yhat)/sqrt(N))
        sqnrm += abs2(dataset[i,end]/sqrt(N))
    end
    sqrt(err/sqnrm)
end

function grid_search2(dataset::Matrix{Float64}, k::Int, train_factory::Function, evaluate::Function, lbl1::String, rng1::Vector{Float64}, lbl2::String, rng2::Vector{Float64}, verbose::Bool)
# grid search for two hyper-parameters using k-fold cross-validation

    N, m = size(dataset)
    d = m - 1

    # generate k random partitions of the dataset
    # into test and training data
    Nk = convert(Int,floor(N/k))
    IDX1 = zeros(Int,(k-1)*Nk,k)
    IDX2 = zeros(Int,Nk,k)
    for i=1:k
        IDX1[:,i] = [(1:(i-1)*Nk); ((i*Nk+1):k*Nk)]
        IDX2[:,i] = (i-1)*Nk+1:i*Nk
    end

    # iterate over all possible combinations for the two hyper-parameters
    best_cv_err = Inf
    best_param1 = NaN
    best_param2 = NaN
    if verbose
        @printf("\n")
    end
    for param1 = rng1
        for param2 = rng2
            if verbose
                @printf("  testing %s=%e, %s=%e ...",lbl1,param1,lbl2,param2)
            end
            # compute the cross-validation error
            cv_err = 0.0
            for i=1:k
                T = train_factory(dataset[IDX1[:,i],:],param1,param2)
                fun = x::Vector{Float64} -> evaluate(T,x)
                cv_err += test(fun,dataset[IDX2[:,i],:])/k
            end
            if verbose
                @printf(" error = %f\n",cv_err)
            end
            # keep current hyper-parameters if lowest error so far
            if cv_err<best_cv_err
                best_cv_err = cv_err
                best_param1 = param1
                best_param2 = param2
            end
        end
    end
    if verbose
        @printf("  elapsed time:")
    end
    if best_param1 == minimum(rng1) || best_param1 == maximum(rng1)
        @warn("TestAndTune.grid_search2(...): best_param1==max/min(rng1)")
    end
    if best_param2 == minimum(rng2) || best_param2 == maximum(rng2)
        @warn("TestAndTune.grid_search2(...): best_param2==max/min(rng2)")
    end

    best_param1, best_param2, best_cv_err
end

end
