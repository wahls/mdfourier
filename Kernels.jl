# Copyright 2014 Sander Wahls
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module Kernels

export gaussian

function gaussian(sigma::Float64)
    (x::Vector{Float64},y::Vector{Float64}) -> begin
        tmp::Float64 = 0.0
        for i=1:length(x)
            tmp += abs2(x[i] - y[i])
        end
        exp(-sigma*tmp/sqrt(length(x)))
    end
end

end
