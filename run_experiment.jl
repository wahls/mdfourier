# Copyright 2014, 2019 Sander Wahls
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

using Printf
using Random
using InteractiveUtils

function run_experiment(logfile, dataset, Ntrain, Ntest, Nruns, Nguesses, k, rng_lam, rng_sig, r, m, rng_p, random_seed=0)
# benchmarks kernel ridge regression vs multidimensional Fourier regression vs random Fourier regression on a dataset

        Random.seed!(random_seed)

        InteractiveUtils.versioninfo(logfile)
        now = Base.Libc.TmStruct(time())
        @printf(logfile,"Date and time: %d-%d-%d, %d:%d:%d\n",1900+now.year,1+now.month,now.mday,now.hour,now.min,now.sec)

        function print_rng(lbl,rng)
            @printf(logfile,"rng_%s = [\n",lbl);
            for i=1:length(rng)
                @printf(logfile,"  %e\n",rng[i]);
            end
            @printf(logfile,"]\n");
        end

        @printf(logfile,"Ntrain = %d\nNtest = %d\nNruns = %d\nNguesses = %d\nk = %d\nrandom_seed = %d\n",Ntrain,Ntest,Nruns,Nguesses,k,random_seed)
        print_rng("lam",rng_lam)
        print_rng("sig",rng_sig)
        print_rng("p",rng_p)
        @printf(logfile,"m = %d\nr = %d\n",m,r);

        N,q = size(dataset)
        verbose = false
        perm = zeros(Int,N,Nruns)
        for run = 1:Nruns
            perm[:,run] = randperm(N)
        end

        ### evaluate kernel ridge regression ###
        @printf(logfile,"\nKERNEL RIDGE REGRESSION\n\n")

        train_factory = (dataset,sigma,lambda) -> begin
            N,q = size(dataset)
            kernel = Kernels.gaussian(sigma)
            pred = KernelRidgeRegression.Predictor(N,q-1,kernel)
            KernelRidgeRegression.train(pred,dataset,lambda)
            pred
        end

        mean_cv_err = 0.0
        mean_test_err = 0.0
        for run = 1:Nruns
            @printf(logfile," RUN %d OF %d\n",run,Nruns); flush(logfile)
            idx1 = perm[1:Ntrain,run]
            idx2 = perm[Ntrain+1:Ntrain+Ntest,run]
            @printf(logfile,"  testing parameters ...");
            t = time()
            sigma,lambda,cv_err = TestAndTune.grid_search2(dataset[idx1,:],k,train_factory,KernelRidgeRegression.evaluate,"sigma",rng_sig,"lambda",rng_lam,verbose)
            t = time() - t
            @printf(logfile, " %f secs\n",t)
            @printf(logfile,"  sigma: %e\n  lambda: %e\n",sigma,lambda); flush(logfile)
            @printf(logfile,"  cv error: %f\n  test error:",cv_err); flush(logfile)
            pred = train_factory(dataset[idx1,:],sigma,lambda)
            fun = x::Vector{Float64} -> KernelRidgeRegression.evaluate(pred,x)
            test_err = TestAndTune.test(fun,dataset[idx2,:])
            @printf(logfile," %f\n",test_err); flush(logfile)
            mean_cv_err += cv_err/Nruns
            mean_test_err += test_err/Nruns
        end
        @printf(logfile," MEAN VALUES\n  cv error: %f\n  test error: %f\n",mean_cv_err,mean_test_err); flush(logfile)

        ### evaluate multidimensional Fourier regression ###
        @printf(logfile,"\nMULTIDIMENSIONAL FOURIER REGRESSION\n\n")

        #lambda = 100*eps()
        niter = 10

        train_factory = (dataset,p,lambda) -> begin
            N,q = size(dataset)
            pred = MDFourierRegression.Predictor(convert(Int,m),p,r,q-1)
            MDFourierRegression.train(pred,dataset,lambda,niter,Nguesses)
            pred
        end

        mean_cv_err = 0.0
        mean_test_err = 0.0
        D = 0.0
        for run = 1:Nruns
            @printf(logfile," RUN %d OF %d:\n",run,Nruns);
            idx1 = perm[1:Ntrain,run]
            idx2 = perm[Ntrain+1:Ntrain+Ntest,run]
            @printf(logfile,"  testing parameters ..."); flush(logfile)
            t = time()
            p, lambda, cv_err = TestAndTune.grid_search2(dataset[idx1,:],k,train_factory,MDFourierRegression.evaluate,"p",rng_p,"lambda",rng_lam,verbose)
            t = time() - t
            @printf(logfile, " %f secs\n",t)
            @printf(logfile,"  m: %d\n  p: %f\n  r: %d\n  lambda: %e\n",m,p,r,lambda);
            @printf(logfile,"  cv error: %f\n  test error:",cv_err); flush(logfile)
            pred = train_factory(dataset[idx1,:],p,lambda)
            fun = x::Vector{Float64} -> MDFourierRegression.evaluate(pred,x)
            test_err = TestAndTune.test(fun,dataset[idx2,:])
            @printf(logfile," %f\n",test_err); flush(logfile)
            D += (d-2)*m*r*r+2*m*r
            mean_cv_err += cv_err/Nruns
            mean_test_err += test_err/Nruns
        end
        D = convert(Int,ceil(D/Nruns))
        @printf(logfile," MEAN VALUES\n  cv error: %f\n  test error: %f\n",mean_cv_err,mean_test_err); flush(logfile)

        ### evaluate random Fourier regression
        @printf(logfile,"\nRANDOM FOURIER REGRESSION\n\n")
        mean_cv_err = 0.0
        mean_test_err = 0.0
        for run = 1:Nruns
            @printf(logfile," RUN %d OF %d\n",run,Nruns);
            train_factory = (dataset,sigma,lambda) -> begin
                N,q = size(dataset)
                pred = RandomFourierRegression.Predictor(D,q-1)
                RandomFourierRegression.train(pred,dataset,sigma,lambda,Nguesses)
                pred
            end
            idx1 = perm[1:Ntrain,run]
            idx2 = perm[Ntrain+1:Ntrain+Ntest,run]
            @printf(logfile,"  testing parameters ..."); flush(logfile)
            t = time()
            sigma,lambda,cv_err = TestAndTune.grid_search2(dataset[idx1,:],k,train_factory,RandomFourierRegression.evaluate,"sigma",rng_sig,"lambda",rng_lam,verbose)
            t = time() - t
            @printf(logfile, " %f secs\n",t)
            @printf(logfile,"  sigma: %e\n  lambda: %e\n  D: %d\n",sigma,lambda,D);
            @printf(logfile,"  cv error: %f\n  test error:",cv_err); flush(logfile)
            pred = train_factory(dataset[idx1,:],sigma,lambda)
            fun = x::Vector{Float64} -> RandomFourierRegression.evaluate(pred,x)
            test_err = TestAndTune.test(fun,dataset[idx2,:])
            @printf(logfile," %f\n",test_err); flush(logfile)
            mean_cv_err += cv_err/Nruns
            mean_test_err += test_err/Nruns
        end
        @printf(logfile," MEAN VALUES\n  cv error: %f\n  test error: %f\n",mean_cv_err,mean_test_err); flush(logfile)
end
