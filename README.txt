Note (Aug. 2019): The code in this folder has been updated to
work with recent versions of Julia (~1.1). The original code
can be obtained by checking out commit 42f4769.

--- Original README -----------------------------------------

This folder contains the source code that has been used
to produce the numerical results given in the paper

"Learning Multidimensional Fourier Series With Tensor Trains"

by S. Wahls, V. Koivunen, H. V. Poor and M. Verhaegen that
will be presented at the IEEE GlobalSIP'14 conference in
December 2014 in Atlanta, GA, USA.

The source code has been written in the Julia programming
language. Please see http://julialang.org/ for details.

The source code is provided under version 2.0 of the Apache
license. Please see the file APACHE_LICENSE_20.txt.
