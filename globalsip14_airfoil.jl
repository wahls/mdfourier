# Copyright 2014, 2019 Sander Wahls
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

using DelimitedFiles

include("Kernels.jl")
include("KernelRidgeRegression.jl")
include("MDFourierRegression.jl")
include("RandomFourierRegression.jl")
include("TestAndTune.jl")
include("run_experiment.jl")

dataset = []
try
  global dataset = readdlm("dataset_airfoil.csv", ',')
catch
  error("Could not open the file dataset_airfoil.csv.\n\nPlease download the dataset from\n\nhttp://archive.ics.uci.edu/ml/datasets/Airfoil+Self-Noise\n\nand convert it to comma-separated values (CSV).\n")
end

N, m = size(dataset)
d = m - 1
dataset[1:N,1:d] = dataset[1:N,1:d]/(2*maximum(abs.(dataset[1:N,1:d])))
Ntrain = convert(Int,ceil(0.7*N))
Ntest = N - Ntrain
Nruns = 10
Nguesses = 3
k = 5

lam = 10.0.^(-18:-4)
sig = 2.0.^(-8:28)
r = 2
m = 12.0
p = 1 .+ 2.0.^(-4:4)

fn = @sprintf("globalsip14_airfoil_%d.txt",time())
@printf("please wait ... logging to file %s\n",fn)
logfile = open(fn,"w")
#logfile = STDOUT
run_experiment(logfile, dataset, Ntrain, Ntest, Nruns, Nguesses, k, lam, sig, r, m, p)
close(logfile)
exit()
