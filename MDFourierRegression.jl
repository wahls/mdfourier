# Copyright 2014, 2019 Sander Wahls
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module MDFourierRegression

export Predictor, train, evaluate

using LinearAlgebra

struct Predictor
  m::Int
  p::Float64
  r::Int
  d::Int
  CORES::Matrix{Complex{Float64}}

  function Predictor(m::Int, p::Float64, r::Int, d::Int)
    if m<=0 || m%2!=0 || p<=0 || r<=0
      throw(DomainError)
    end
    CORES = zeros(Complex{Float64},m*r*r,d)
    new(m,p,r,d,CORES)
  end
end

struct Storage
  MAT::Matrix{Complex{Float64}}
  L::Matrix{Complex{Float64}}
  R::Array{Complex{Float64},3}
  A::Array{Complex{Float64},3}
  B::Matrix{Complex{Float64}}
  BtB::Matrix{Complex{Float64}}
  S::Matrix{Float64}
  lambda::Float64
end

function train(pred::Predictor, dataset::Matrix{Float64}, lambda::Float64, niter::Int, nguesses::Int)
  # check input dimensions
  N,q = size(dataset)
  if lambda<0 || niter<=0 || nguesses<=1 || N<=0 || q!=pred.d+1
    throw(DomainError)
  end

  # this is just to simplify notation
  m = pred.m
  p = pred.p
  r = pred.r
  d = pred.d

  # reserve memory for intermediate computations
  MAT = zeros(Complex{Float64},N+m*r*r,m*r*r)
  L = zeros(Complex{Float64},r,N)
  R = zeros(Complex{Float64},r,N,d-1)
  A = zeros(Complex{Float64},r,r,d-1)
  B = zeros(Complex{Float64},r,r)
  BtB = zeros(Complex{Float64},r,r)
  S = zeros(m,m)
  for l = 1:m
    for s = 1:m
      S[l,s] = sinc((l-s)/p)
    end
  end
  S = robust_chol(S,:U)
  stor = Storage(MAT,L,R,A,B,BtB,S,lambda)

  # train with different random initial guesses, keep best result
  best_err = Inf
  best_CORES = zeros(Complex{Float64},m*r*r,d)
  for guess = 1:nguesses

    # create a random left-orthonormal initial guess
    pred.CORES[:,:] = randn(m*r*r,d)+im*randn(m*r*r,d)
    for k = 1:d
      normalize_core!(pred,k)
    end

    # perform niter iterations of the algorithm
    for iter = 1:niter
      prebuffer_R!(pred,stor,dataset)
      prebuffer_A!(pred,stor)
      for k = 1:d-1
        update_L!(pred,stor,k,dataset)
        update_B!(pred,stor,k)
        update_core!(pred,stor,k,dataset)
        normalize_core!(pred,k)
      end
      update_L!(pred,stor,d,dataset)
      update_B!(pred,stor,d)
      update_core!(pred,stor,d,dataset)
    end

    # save if best performance so far
    test_err = training_error(pred,stor,dataset)
    isnan(test_err) && error("training failed, test_err = NaN")
    if test_err<best_err
      best_err = test_err
      best_CORES[:] = pred.CORES
    end
  end
  pred.CORES[:] = best_CORES
  nothing
end

function evaluate(pred::Predictor, x::Vector{Float64})
  # via Lemma 3: y = GAMMA1(z1)x...xGAMMAd(zd)
  y = Matrix{Complex{Float64}}(I,1,1)
  z = zeros(Complex{Float64},pred.m,1)
  tmp = 2*pi*im*Vector(-div(pred.m,2):(div(pred.m,2)-1))/pred.p
  for k = 1:pred.d
    z = [exp(tmp[i]*x[k]) for i=1:pred.m]
    y = y*GAMMA(pred,z,k)
  end
  real(y[1]) # [1] avoids returning Float64 array of size 1x1 instead of Float64
end

function normalize_core!(pred::Predictor, k::Int)
  # orthonormalizes the left-unfolding of the k-th core using modified Gram-Schmidt
  sz1,sz2 = core_size(pred,k)
  for j = 1:sz2
    offset1 = (j-1)*pred.m*sz1
    nrm = 0.0
    for i = 1:pred.m*sz1
      nrm += abs2(pred.CORES[offset1+i,k])
    end
    nrm = sqrt(nrm)
    for i = 1:pred.m*sz1
      pred.CORES[offset1+i,k] /= nrm
    end
    for jj = (j+1):sz2
      offset2 = (jj-1)*pred.m*sz1
      dotpr = 0.0
      for i = 1:pred.m*sz1
        dotpr += pred.CORES[offset1+i,k]'*pred.CORES[offset2+i,k]
      end
      for i = 1:pred.m*sz1
        pred.CORES[offset2+i,k] -= dotpr*pred.CORES[offset1+i,k]
      end
    end
  end
end

function update_core!(pred::Predictor, stor::Storage, k::Int, dataset::Matrix{Float64})
  # minimize the empirical risk w.r.t. the k-th core by solving Eq. (16)
  N, q = size(dataset)
  d = q - 1
  sz1,sz2 = core_size(pred,k)
  sz = pred.m*sz1*sz2
  tmp = 2*pi*im*Vector(-div(pred.m,2):(div(pred.m,2)-1))/pred.p
  z = zeros(Complex{Float64},pred.m,1)
  Hk = kron(z',eye(sz1))

  # construct the coefficient matrix
  if k==1
    for i = 1:N
      z[:] = exp.(tmp*dataset[i,k])
      Hk[:,:] = kron(z',eye(sz1))
      stor.MAT[i,1:sz] = kron(transpose(stor.R[:,i,1]),Hk)
    end
    stor.MAT[N+1:N+sz,1:sz] = sqrt(N*stor.lambda)*kron(transpose(stor.A[:,:,k]),stor.S)
  elseif k==d
    for i = 1:N
      z[:] = exp.(tmp*dataset[i,k])
      Hk[:,:] = kron(z',eye(sz1))
      stor.MAT[i,1:sz] = transpose(stor.L[:,i])*Hk
    end
    stor.MAT[N+1:N+sz,1:sz] = sqrt(N*stor.lambda)*kron(stor.S,stor.B)
  else
    for i = 1:N
      z[:] = exp.(tmp*dataset[i,k])
      Hk[:,:] = kron(z',eye(sz1))
      stor.MAT[i,1:sz] = kron(transpose(stor.R[:,i,k]),transpose(stor.L[:,i])*Hk)
    end
    stor.MAT[N+1:N+sz,1:sz] = sqrt(N*stor.lambda)*kron(transpose(stor.A[:,:,k]),kron(stor.S,stor.B))
  end

  # solve the regularized least-squares problem
  tmp2 = stor.MAT[1:N+sz,1:sz]\[dataset[:,end]; zeros(sz,1)]

  pred.CORES[1:sz,k] = tmp2[1:sz,:]
end

function training_error(pred::Predictor, stor::Storage, dataset::Matrix{Float64})
  # call only directly after update_core! with k==d
  N = size(dataset,1)
  sz = pred.m*pred.r
  norm(dataset[:,end]-stor.MAT[1:N,1:sz]*pred.CORES[1:sz,pred.d])/norm(dataset[:,end])
end

function GAMMA(pred::Predictor, z::Vector{Complex{Float64}}, k::Int)
  # returns GAMMAk(z) = sum_l Gkl*z(l)'
  sz1,sz2 = core_size(pred,k)
  GAM = zeros(Complex{Float64},sz1,sz2)
  for l = 1:pred.m
    GAM += Gkl(pred,k,l)*z[l]'
  end
  GAM
end

function Gkl(pred::Predictor, k::Int, l::Int)
  # returns the l-th element of the k-th core
  sz1,sz2 = core_size(pred,k)
  G = zeros(Complex{Float64},sz1,sz2)
  idx = (l-1)*sz1+1
  for j = 1:sz2
    for i = 1:sz1
      G[i,j] = pred.CORES[idx,k]
      idx += 1
    end
    idx += (pred.m-1)*sz1
  end
  G
end

function update_L!(pred::Predictor, stor::Storage, k::Int, dataset::Matrix{Float64})
  # updates the Lk-1(zi) saved in stor.L to Lk(zi) for i=1,...,N
  N, q = size(dataset)
  tmp = 2*pi*im*Vector(-div(pred.m,2):(div(pred.m,2)-1))/pred.p
  z = zeros(Complex{Float64},pred.m,1)

  if k==2
    for i = 1:N
      z = exp.(tmp*dataset[i,1])
      stor.L[:,i] = transpose(GAMMA(pred,z,1))
    end
  elseif k>2
    for i = 1:N
      z = exp.(tmp*dataset[i,k-1])
      stor.L[:,i] = transpose(GAMMA(pred,z,k-1))*stor.L[:,i]
    end
  end
end

function prebuffer_R!(pred::Predictor, stor::Storage, dataset::Matrix{Float64})
  # computes Rk(zi) for all k and i and saves that in stor.R
  N, q = size(dataset)
  d = q-1
  tmp = 2*pi*im*Vector(-div(pred.m,2):(div(pred.m,2)-1))/pred.p
  z = zeros(Complex{Float64},pred.m,1)
  for i = 1:N
    z = exp.(tmp*dataset[i,d])
    stor.R[:,i,d-1] = GAMMA(pred,z,d)
    for kk = (d-2):-1:1
      z = exp.(tmp*dataset[i,kk+1])
      stor.R[:,i,kk] = GAMMA(pred,z,kk+1)*stor.R[:,i,kk+1]
    end
  end
end

function update_B!(pred::Predictor, stor::Storage, k::Int)
  # updates Bk-1 saved in stor.B to Bk
  if k==1
    return
  end
  Idp = (-div(pred.m,2):(div(pred.m,2)-1))/pred.p
  TMP2 = zeros(Complex{Float64},pred.r,pred.r)
  TMP3 = zeros(Complex{Float64},pred.r,pred.r)
  if k==2
    fill!(stor.BtB,0.0im)
    for l = 1:pred.m
      stor.BtB[:,:] += Gkl(pred,1,l)'*Gkl(pred,1,l)
      for s = l+1:pred.m
        TMP3 = Gkl(pred,1,s)'*Gkl(pred,1,l)*sinc(Idp[l]-Idp[s])
        stor.BtB[:,:] += TMP3+TMP3'
      end
    end
    stor.B[:,:] = robust_chol(stor.BtB,:U)
  else
    for l = 1:pred.m
      TMP2 += Gkl(pred,k-1,l)'*stor.BtB*Gkl(pred,k-1,l)
      for s = l+1:pred.m
        TMP3 = Gkl(pred,k-1,s)'*stor.BtB*Gkl(pred,k-1,l)*sinc(Idp[l]-Idp[s])
        TMP2 += TMP3+TMP3'
      end
    end
    stor.BtB[:,:] = TMP2
    stor.B[:,:] = robust_chol(TMP2,:U)
  end
end

function prebuffer_A!(pred::Predictor, stor::Storage)
    # computes Ak for all k saves them in stor.A
    Idp = (-div(pred.m,2):(div(pred.m,2)-1))/pred.p
    TMP1 = zeros(Complex{Float64},pred.r,pred.r)
    TMP2 = zeros(Complex{Float64},pred.r,pred.r)
    TMP3 = zeros(Complex{Float64},pred.r,pred.r)
    for l = 1:pred.m
        TMP1 += Gkl(pred,pred.d,l)*Gkl(pred,pred.d,l)'
        for s = l+1:pred.m
            TMP3 = Gkl(pred,pred.d,l)*Gkl(pred,pred.d,s)'*sinc(Idp[l]-Idp[s])
            TMP1 += TMP3+TMP3'
        end
    end
    stor.A[:,:,pred.d-1] = robust_chol(TMP1,:L)
    for k = pred.d-1:-1:2
        fill!(TMP2,0.0im)
        for l = 1:pred.m
            TMP2 += Gkl(pred,k,l)*TMP1*Gkl(pred,k,l)'
            for s = l+1:pred.m
                TMP3 = Gkl(pred,k,l)*TMP1*Gkl(pred,k,s)'*sinc(Idp[l]-Idp[s])
                TMP2 += TMP3+TMP3'
            end
        end
        TMP1 .= TMP2
        stor.A[:,:,k-1] = robust_chol(TMP1,:L)
    end
end

function core_size(pred::Predictor, k::Int)
  # size of the k-th core
  k==1 ? 1 : pred.r, k<pred.d ? pred.r : 1
end

function robust_chol(A,lower_or_upper::Symbol)
  # chol failed too often because matrices where not exactly
  # symmetric pos-def; this function is more robust
  try
    return Matrix(chol(A,lower_or_upper))
  catch
  end
  if prod(size(A))==1
    return sqrt(abs(A[1,1]))*ones(1,1)
  end
  eta = sqrt(eps())
  n = 2*ceil(-log10(eta))
  eta = eta*(norm(A)+eta)
  for i = 1:n
    try
      return Matrix(chol(0.5*(A+A')+eta*eye(size(A,1)),lower_or_upper))
    catch
    end
    eta *= 10
  end
  println("\nA =")
  println(A)
  error("Cholesky decomposition failed")
end

# Below: replace functions that were depreceated in Julia 1.0

function chol(A,lower_or_upper)
    if lower_or_upper == :L
        return cholesky(A).L
    else
        return cholesky(A).U
    end
end

function eye(sz)
    if length(sz) == 1
        return Matrix(1.0I, sz, sz)
    else
        return Matrix(1.0I, sz[1], sz[2])
    end
end

end
