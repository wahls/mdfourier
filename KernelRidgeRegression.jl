# Copyright 2014, 2019 Sander Wahls
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module KernelRidgeRegression

export Predictor, train, evaluate

using LinearAlgebra

struct Predictor
  X::Matrix{Float64}
  N::Int
  d::Int
  alpha::Vector{Float64}
  kernel::Function

  function Predictor(N::Int, d::Int, kernel::Function)
    if N<1 || d<1
      throw(DomainError)
    end
    new(zeros(d,N),N,d,zeros(N),kernel)
  end
end

function train(pred::Predictor, dataset::Matrix{Float64}, lambda::Float64)
  # check inputs
  N,q = size(dataset)
  d = q-1
  if N!=pred.N || d!=pred.d
    throw(DomainError)
  end

  # construct kernel matrix
  K = zeros(N,N)
  for i = 1:N
    for j = 1:N
      K[i,j] = pred.kernel(vec(dataset[i,1:d]),vec(dataset[j,1:d]))
    end
  end

  # compute weight vector
  pred.X[:] = transpose(dataset[:,1:d])
  try
    pred.alpha[:] = (K+N*lambda*Matrix(1.0I,N,N))\dataset[:,end]
  catch
    @warn("KernelRidgeRegression.Train(...): failed to solve linear system")
  end

  return nothing
end

function evaluate(pred::Predictor,x::Vector{Float64})
  y = 0.0
  for i = 1:pred.N
    y += pred.alpha[i]*pred.kernel(x,vec(pred.X[:,i]))
  end
  y
end

end
