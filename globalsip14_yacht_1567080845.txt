Julia Version 1.1.1
Commit 55e36cc308 (2019-05-16 04:10 UTC)
Platform Info:
  OS: Linux (x86_64-pc-linux-gnu)
  CPU: Intel(R) Core(TM) i7-4600U CPU @ 2.10GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-6.0.1 (ORCJIT, haswell)
Date and time: 2019-8-29, 14:14:15
Ntrain = 216
Ntest = 92
Nruns = 10
Nguesses = 3
k = 5
random_seed = 0
rng_lam = [
  1.000000e-28
  1.000000e-27
  1.000000e-26
  1.000000e-25
  1.000000e-24
  1.000000e-23
  1.000000e-22
  1.000000e-21
  1.000000e-20
  1.000000e-19
  1.000000e-18
  1.000000e-17
  1.000000e-16
  1.000000e-15
  1.000000e-14
]
rng_sig = [
  3.906250e-03
  7.812500e-03
  1.562500e-02
  3.125000e-02
  6.250000e-02
  1.250000e-01
  2.500000e-01
  5.000000e-01
  1.000000e+00
  2.000000e+00
  4.000000e+00
  8.000000e+00
  1.600000e+01
  3.200000e+01
  6.400000e+01
  1.280000e+02
  2.560000e+02
  5.120000e+02
  1.024000e+03
  2.048000e+03
  4.096000e+03
  8.192000e+03
  1.638400e+04
  3.276800e+04
  6.553600e+04
  1.310720e+05
  2.621440e+05
  5.242880e+05
  1.048576e+06
  2.097152e+06
  4.194304e+06
  8.388608e+06
  1.677722e+07
  3.355443e+07
  6.710886e+07
  1.342177e+08
  2.684355e+08
]
rng_p = [
  1.062500e+00
  1.125000e+00
  1.250000e+00
  1.500000e+00
  2.000000e+00
  3.000000e+00
  5.000000e+00
  9.000000e+00
  1.700000e+01
]
m = 6
r = 1

KERNEL RIDGE REGRESSION

 RUN 1 OF 10
  testing parameters ... 26.402458 secs
  sigma: 8.000000e+00
  lambda: 1.000000e-16
  cv error: 0.045204
  test error: 0.043292
 RUN 2 OF 10
  testing parameters ... 24.756900 secs
  sigma: 8.000000e+00
  lambda: 1.000000e-16
  cv error: 0.045372
  test error: 0.038045
 RUN 3 OF 10
  testing parameters ... 25.229302 secs
  sigma: 8.000000e+00
  lambda: 1.000000e-16
  cv error: 0.049658
  test error: 0.034887
 RUN 4 OF 10
  testing parameters ... 25.661145 secs
  sigma: 8.000000e+00
  lambda: 1.000000e-16
  cv error: 0.050019
  test error: 0.036027
 RUN 5 OF 10
  testing parameters ... 27.343750 secs
  sigma: 8.000000e+00
  lambda: 1.000000e-16
  cv error: 0.057844
  test error: 0.035463
 RUN 6 OF 10
  testing parameters ... 30.709292 secs
  sigma: 6.400000e+01
  lambda: 1.000000e-14
  cv error: 0.060972
  test error: 0.037752
 RUN 7 OF 10
  testing parameters ... 26.679085 secs
  sigma: 8.000000e+00
  lambda: 1.000000e-16
  cv error: 0.048459
  test error: 0.043072
 RUN 8 OF 10
  testing parameters ... 25.148001 secs
  sigma: 8.000000e+00
  lambda: 1.000000e-16
  cv error: 0.042564
  test error: 0.034081
 RUN 9 OF 10
  testing parameters ... 25.387439 secs
  sigma: 1.600000e+01
  lambda: 1.000000e-16
  cv error: 0.044903
  test error: 0.037283
 RUN 10 OF 10
  testing parameters ... 28.222727 secs
  sigma: 1.600000e+01
  lambda: 1.000000e-16
  cv error: 0.058013
  test error: 0.082083
 MEAN VALUES
  cv error: 0.050301
  test error: 0.042198

MULTIDIMENSIONAL FOURIER REGRESSION

 RUN 1 OF 10:
  testing parameters ... 89.246210 secs
  m: 6
  p: 1.250000
  r: 1
  lambda: 1.000000e-15
  cv error: 0.052033
  test error: 0.052882
 RUN 2 OF 10:
  testing parameters ... 83.747762 secs
  m: 6
  p: 1.250000
  r: 1
  lambda: 1.000000e-23
  cv error: 0.047373
  test error: 0.045137
 RUN 3 OF 10:
  testing parameters ... 89.277424 secs
  m: 6
  p: 1.125000
  r: 1
  lambda: 1.000000e-23
  cv error: 0.044717
  test error: 0.057420
 RUN 4 OF 10:
  testing parameters ... 86.647169 secs
  m: 6
  p: 1.062500
  r: 1
  lambda: 1.000000e-26
  cv error: 0.043753
  test error: 0.048723
 RUN 5 OF 10:
  testing parameters ... 85.307538 secs
  m: 6
  p: 1.125000
  r: 1
  lambda: 1.000000e-22
  cv error: 0.043638
  test error: 0.045539
 RUN 6 OF 10:
  testing parameters ... 89.015965 secs
  m: 6
  p: 2.000000
  r: 1
  lambda: 1.000000e-28
  cv error: 0.049803
  test error: 0.047964
 RUN 7 OF 10:
  testing parameters ... 85.833642 secs
  m: 6
  p: 2.000000
  r: 1
  lambda: 1.000000e-21
  cv error: 0.048278
  test error: 0.052880
 RUN 8 OF 10:
  testing parameters ... 85.876355 secs
  m: 6
  p: 3.000000
  r: 1
  lambda: 1.000000e-22
  cv error: 0.045551
  test error: 0.070471
 RUN 9 OF 10:
  testing parameters ... 89.824625 secs
  m: 6
  p: 2.000000
  r: 1
  lambda: 1.000000e-23
  cv error: 0.043786
  test error: 0.048744
 RUN 10 OF 10:
  testing parameters ... 87.014334 secs
  m: 6
  p: 3.000000
  r: 1
  lambda: 1.000000e-21
  cv error: 0.054262
  test error: 0.047595
 MEAN VALUES
  cv error: 0.047319
  test error: 0.051736

RANDOM FOURIER REGRESSION

 RUN 1 OF 10
  testing parameters ... 2.609474 secs
  sigma: 3.906250e-03
  lambda: 1.000000e-15
  D: 36
  cv error: 0.258449
  test error: 0.214694
 RUN 2 OF 10
  testing parameters ... 2.557734 secs
  sigma: 3.906250e-03
  lambda: 1.000000e-15
  D: 36
  cv error: 0.257393
  test error: 0.228695
 RUN 3 OF 10
  testing parameters ... 2.606187 secs
  sigma: 3.906250e-03
  lambda: 1.000000e-21
  D: 36
  cv error: 0.262819
  test error: 0.256315
 RUN 4 OF 10
  testing parameters ... 2.566032 secs
  sigma: 3.906250e-03
  lambda: 1.000000e-15
  D: 36
  cv error: 0.266771
  test error: 0.269594
 RUN 5 OF 10
  testing parameters ... 2.459847 secs
  sigma: 1.562500e-02
  lambda: 1.000000e-14
  D: 36
  cv error: 0.248210
  test error: 0.257194
 RUN 6 OF 10
  testing parameters ... 2.544579 secs
  sigma: 3.906250e-03
  lambda: 1.000000e-15
  D: 36
  cv error: 0.261550
  test error: 0.243416
 RUN 7 OF 10
  testing parameters ... 2.603340 secs
  sigma: 3.125000e-02
  lambda: 1.000000e-14
  D: 36
  cv error: 0.240946
  test error: 0.292664
 RUN 8 OF 10
  testing parameters ... 2.712618 secs
  sigma: 7.812500e-03
  lambda: 1.000000e-14
  D: 36
  cv error: 0.275234
  test error: 0.220229
 RUN 9 OF 10
  testing parameters ... 2.577017 secs
  sigma: 3.906250e-03
  lambda: 1.000000e-15
  D: 36
  cv error: 0.248313
  test error: 0.263826
 RUN 10 OF 10
  testing parameters ... 2.546561 secs
  sigma: 3.906250e-03
  lambda: 1.000000e-25
  D: 36
  cv error: 0.260712
  test error: 0.276402
 MEAN VALUES
  cv error: 0.258040
  test error: 0.252303
