# Copyright 2014, 2019 Sander Wahls
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module RandomFourierRegression

export Predictor, train, evaluate

using LinearAlgebra

struct Predictor
  Omega::Matrix{Float64}
  b::Vector{Float64}
  w::Vector{Float64}
  D::Int
  d::Int

  function Predictor(D::Int, d::Int)
    if D<1 || d<1
      throw(DomainError)
    end
    new(zeros(D,d),zeros(D),zeros(D),D,d)
  end
end

function train(pred::Predictor, dataset::Matrix{Float64}, sigma::Float64, lambda::Float64, nguesses::Int)
  N,q = size(dataset)
  d = q - 1
  @assert(pred.d == d)

  # train with with different random parameters, keep best one
  best_err = Inf
  best_Omega = zeros(pred.D,pred.d)
  best_b = zeros(pred.D)
  best_w = zeros(pred.D)
  for guess = 1:nguesses
    # generate parameters at random
    pred.Omega[:,:] = sqrt(2*sigma)*randn(pred.D,pred.d)
    pred.b[:] = 2*pi*rand(pred.D)
    Z = Zx(pred,Matrix(transpose(dataset[:,1:d])))

    # compute optimal weight and test error
    pred.w[:] = vec((Z*Z'+N*lambda*Matrix(1.0I,pred.D,pred.D))\(Z*dataset[:,end]))
    test_err = norm(dataset[:,end]-Z'*pred.w)/norm(dataset[:,end])

    # keep if lowest test error so far
    if test_err<best_err
      best_err = test_err
      best_Omega[:,:] = pred.Omega
      best_b[:] = pred.b
      best_w[:] = pred.w
    end
  end
  pred.Omega[:,:] = best_Omega
  pred.b[:] = best_b
  pred.w[:] = best_w
  return nothing
end

function Zx(pred::Predictor, X::Matrix{Float64})
  d,N = size(X)
  RET = zeros(pred.D,N)
  for i=1:N
    RET[:,i] = Zx(pred,vec(X[:,i]))
  end
  RET
end

function Zx(pred::Predictor, x::Vector{Float64})
  sqrt(2/pred.D)*cos.(pred.Omega*x + pred.b)
end

function evaluate(pred::Predictor, x::Vector{Float64})
  dot(pred.w,Zx(pred,x))
end

end
